﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CameraRigBehaviour : MonoBehaviour
{
    private Vector3? _mousePos;
    private bool _spinning;

    private void Update()
    {
        var cameraMoveSpeed = .3f;
        if (Input.GetKey(KeyCode.A))
        {
            transform.position -= transform.right * cameraMoveSpeed;
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.position += transform.right * cameraMoveSpeed;
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.position -= transform.forward * cameraMoveSpeed;
        }

        if (Input.GetKey(KeyCode.W))
        {
            transform.position += transform.forward * cameraMoveSpeed;
        }

        if (Input.GetKey(KeyCode.Q))
        {
            StartCoroutine(SpinCoroutine(transform.localEulerAngles + 45 * Vector3.up));
        }

        if (Input.GetKey(KeyCode.E))
        {
            StartCoroutine(SpinCoroutine(transform.localEulerAngles - 45 * Vector3.up));
        }

        if (Input.GetMouseButton(2))
        {
            var newMousePos = Input.mousePosition;
            if (_mousePos != null)
            {
                var prevMouse = (Vector3)_mousePos;
                transform.position -= (transform.right * (newMousePos.x - prevMouse.x) + transform.forward * (newMousePos.y - prevMouse.y) * 1.4f) * .03f;
            }

            _mousePos = newMousePos;
        }
        else
        {
            _mousePos = null;
        }

        var scroll = Input.mouseScrollDelta;
        if (Mathf.Abs(scroll.y) > Mathf.Epsilon)
        {
            Camera.main.orthographicSize /= Mathf.Pow(1.1f, scroll.y);
        }
    }

    private IEnumerator SpinCoroutine(Vector3 targetEulerAngles)
    {
        if (_spinning)
        {
            yield break;
        }

        _spinning = true;
        var target = Quaternion.Euler(targetEulerAngles);
        for (var i = 0; i < 15; i++)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, target, i / 15f);
            yield return null;
        }

        transform.rotation = target;
        _spinning = false;
    }
}
