﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Constants
{
    public static string GroundLayer = "Ground";
    public static string WallsLayer = "Walls";
    public static string CharacterLayer = "Character";
    public static string TreasureLayer = "Treasure";
    public static string EnemyLayer = "Enemy";
    public static string ExitTag = "Exit";
    public static string TargetLayer = "Target";
}
