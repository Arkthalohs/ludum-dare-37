﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class EnemyViewModel : EntityViewModel
{
    public float FieldOfViewCone = 45;
    public float SightRange = 7;
    public List<EnemyCommand> PatrolCommands;
    public int CommandIndex;
    public bool RecalculateVisibility;
    public GuardAIState AIState;
    public SpriteRenderer SpottedIcon;

    private ParticleSystem _spottedIconPoof;
    private PlayerTurnViewModel _playerTurn;
    private CharacterBehaviour _characterInSights;
    private Vector3 _lastSeenPlayerLocation;

    public enum GuardAIState
    {
        Patrolling,
        CharacterInSights,
        Searching
    }

    public override void Initialize(Transform character)
    {
        _playerTurn = GameObject.Find("Game Controller").GetComponent<TurnControllerBehaviour>().PlayerTurn;
        base.Initialize(character);
        _spottedIconPoof = Resources.Load<ParticleSystem>("SupriseParticleSystem");
    }

    public void BeginTurn()
    {
        MovementPoints = 4;
    }

    public IEnumerator TakeTurnCoroutine()
    {
        if (EntityCondition == Condition.Unconscious)
        {
            if (_incapacitatedTurns <= 0)
            {
                IncapacitatedCountText.transform.parent.gameObject.SetActive(false);
                EntityCondition = Condition.Conscious;
                _transform.position += Vector3.up;
            }
            else
            {
                _incapacitatedTurns--;
                IncapacitatedCountText.transform.parent.gameObject.SetActive(true);
                IncapacitatedCountText.text = string.Empty + _incapacitatedTurns;
            }
        }

        if (EntityCondition != Condition.Conscious)
        {
            yield break;
        }

        switch (AIState)
        {
            case GuardAIState.Patrolling:
                SpottedIcon.enabled = false;
                yield return PatrolCoroutine();
                break;
            case GuardAIState.CharacterInSights:
                _characterInSights.ViewModel.Attacked(Condition.Dead, -1);
                break;
            case GuardAIState.Searching:
                yield return SearchCoroutine();
                break;
        }
    }

    public void EndTurn()
    {
        MovementPoints = 0;
    }

    public bool CanSee(Vector3 position)
    {
        var distance = position - _transform.position;
        if (distance.magnitude < Mathf.Epsilon)
        {
            return true;
        }

        if (distance.magnitude > SightRange)
        {
            return false;
        }

        var dir = distance.normalized;
        var dotProduct = Vector3.Dot(dir, _transform.forward);
        var angle = Mathf.Acos(dotProduct);
        if (float.IsNaN(angle) && (dir - _transform.forward).magnitude > .05f)
        {
            return false;
        }

        if (angle * Mathf.Rad2Deg > FieldOfViewCone)
        {
            return false;
        }

        RaycastHit hitInfo;
        var hit = Physics.Raycast(_transform.position, distance, out hitInfo, distance.magnitude, LayerMask.GetMask(Constants.WallsLayer), QueryTriggerInteraction.Ignore);
        if (!hit)
        {
            return true;
        }

        // TODO: partial cover--where not moving is hidden, but otherwise visible?

        return false;
    }

    public void NotifyOfSound(Vector3 position)
    {
        if (EntityCondition != Condition.Conscious)
        {
            return;
        }

        _lastSeenPlayerLocation = position;
        if (AIState != GuardAIState.Searching)
        {
            AIState = GuardAIState.Searching;
            SpottedIcon.enabled = true;
            SpawnParticle(_spottedIconPoof, 1f, SpottedIcon.transform.position);
        }
    }

    public void LookForPlayers()
    {
        if (_characterInSights && _characterInSights.ViewModel.EntityCondition == Condition.Conscious)
        {
            if (CanSee(_characterInSights.transform.position))
            {
                _lastSeenPlayerLocation = _characterInSights.transform.position;
                AIState = GuardAIState.CharacterInSights;
                SpottedIcon.enabled = true;
            }
            else
            {
                _characterInSights = null;
                AIState = GuardAIState.Searching;
            }
        }
        else if (_characterInSights)
        {
            _characterInSights = null;
            AIState = GuardAIState.Patrolling;
        }

        var character = LookForCharacter();
        if (character && (!_characterInSights || Vector3.Distance(_transform.position, _characterInSights.transform.position) > Vector3.Distance(_transform.position, character.transform.position)))
        {
            _characterInSights = character;
            _lastSeenPlayerLocation = _characterInSights.transform.position;
            AIState = GuardAIState.CharacterInSights;
            SpottedIcon.enabled = true;
            SpawnParticle(_spottedIconPoof, 1f, SpottedIcon.transform.position);
        }
    }

    public override void Attacked(Condition conditionDealt, int turnsIncapacitated)
    {
        base.Attacked(conditionDealt, turnsIncapacitated);
        IncapacitatedCountText.transform.parent.gameObject.SetActive(true);
        IncapacitatedCountText.text = string.Empty + _incapacitatedTurns;
    }

    protected override bool ShouldCancelMove()
    {
        RecalculateVisibility = true;

        var character = LookForCharacter();
        if (character != null)
        {
            _characterInSights = character;
            _lastSeenPlayerLocation = _characterInSights.transform.position;
            return true;
        }

        return false;
    }

    private IEnumerator PatrolCoroutine()
    {
        var currentCommand = PatrolCommands[CommandIndex];
        var path = Pathfinder.FindPath(_transform.position, currentCommand.MoveToPosition.position);

        if (path == null)
        {
            // No valid path to target
            yield return new WaitForSeconds(.1f);
        }
        else if (path.Length == 0)
        {
            // At the target
            CommandIndex = (CommandIndex + 1) % PatrolCommands.Count;
            yield return new WaitForSeconds(.1f);
        }
        else
        {
            path.TrimToLength(MovementPoints);
            yield return MoveToCoroutine(path, 1, false);
            RecalculateVisibility = true;
        }

        if ((_transform.position - currentCommand.MoveToPosition.position).magnitude < 1)
        {
            // At the target
            CommandIndex = (CommandIndex + 1) % PatrolCommands.Count;
            yield return new WaitForSeconds(.1f);
        }
    }

    private IEnumerator SearchCoroutine()
    {
        var path = Pathfinder.FindPath(_transform.position, _lastSeenPlayerLocation);
        if (path == null)
        {
            AIState = GuardAIState.Patrolling;
            yield return new WaitForSeconds(.1f);
        }
        else if (path.Length == 0)
        {
            // We're at the last seen location, but don't see anything now. It must have been the wind.
            AIState = GuardAIState.Patrolling;
            yield return new WaitForSeconds(.1f);
        }
        else
        {
            path.TrimToLength(MovementPoints);
            yield return MoveToCoroutine(path, 1.5f, false);
            RecalculateVisibility = true;
        }

        LookForPlayers();
    }

    private CharacterBehaviour LookForCharacter()
    {
        foreach (var character in _playerTurn.Team)
        {
            if (CanSee(character.transform.position) && character.ViewModel.EntityCondition == Condition.Conscious)
            {
                return character;
            }
        }

        return null;
    }
}
