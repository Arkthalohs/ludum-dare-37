﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    public MeshFilter SightConeMesh;
    public EnemyViewModel ViewModel;

    private void Start()
    {
        BuildMesh();
        UpdateMeshColors();
    }

    private void Awake()
    {
        ViewModel.Initialize(transform);
    }

    private void Update()
    {
        if (ViewModel.EntityCondition == EntityViewModel.Condition.Conscious)
        {
            if (SightConeMesh == null)
            {
                BuildMesh();
                UpdateMeshColors();
            }

            if (ViewModel.RecalculateVisibility)
            {
                UpdateMeshColors();
                ViewModel.RecalculateVisibility = false;
            }

            ViewModel.LookForPlayers();
        }
        else if (SightConeMesh != null)
        {
            ViewModel.SpottedIcon.enabled = false;
            Destroy(SightConeMesh.gameObject);
            SightConeMesh = null;
        }
    }

    private void UpdateMeshColors()
    {
        var colors = new List<Color>();
        for (var i = 0; i < SightConeMesh.mesh.vertexCount; i++)
        {
            var point = transform.TransformPoint(SightConeMesh.mesh.vertices[i]);
            colors.Add(ViewModel.CanSee(point) && IsGround(point) ? new Color(1, 0, 0, .6f) : new Color(0, 0, 0, 0));
        }

        SightConeMesh.mesh.colors = colors.ToArray();
    }

    private bool IsGround(Vector3 location)
    {
        return Physics.Raycast(new Vector3(location.x, 1.5f, location.z), Vector3.down, 2, LayerMask.GetMask(Constants.GroundLayer));
    }

    private void BuildMesh()
    {
        var triSize = .5f;
        var meshRenderer = new GameObject("Line of Sight");
        SightConeMesh = meshRenderer.AddComponent<MeshFilter>();
        var renderer = meshRenderer.AddComponent<MeshRenderer>();

        SightConeMesh.mesh = new Mesh();
        SightConeMesh.mesh.name = "Vision Cone Mesh";
        renderer.material = new Material(Shader.Find("Custom/VisionCone"));

        var verts = new List<Vector3>();
        var tris = new List<int>();

        var fovRad = ViewModel.FieldOfViewCone * Mathf.Deg2Rad;
        for (var i = 0f; i < ViewModel.SightRange; i += triSize)
        {
            var min = new Vector3(Mathf.Sin(fovRad) / Mathf.Cos(-fovRad) * (i + triSize), 0, i + triSize);
            var prevMin = new Vector3(Mathf.Sin(fovRad) / Mathf.Cos(-fovRad) * (i), 0, i);
            var max = new Vector3(Mathf.Sin(-fovRad) / Mathf.Cos(-fovRad) * (i + triSize), 0, i + triSize);
            var prevMax = new Vector3(Mathf.Sin(-fovRad) / Mathf.Cos(-fovRad) * (i), 0, i);

            var numVerts = verts.Count;
            verts.Add(min);
            verts.Add(prevMin);
            verts.Add(prevMin + Vector3.forward * triSize);
            tris.Add(numVerts);
            tris.Add(numVerts + 1);
            tris.Add(numVerts + 2);

            for (var j = prevMax.x; j < prevMin.x; j += triSize)
            {
                numVerts = verts.Count;
                verts.Add(new Vector3(j, 0, i));
                verts.Add(new Vector3(j, 0, i + triSize));
                verts.Add(new Vector3(j + triSize, 0, i + triSize));
                tris.Add(numVerts);
                tris.Add(numVerts + 1);
                tris.Add(numVerts + 2);
                tris.Add(numVerts);
                tris.Add(numVerts + 2);

                numVerts = verts.Count;
                verts.Add(new Vector3(j + triSize, 0, i));
                tris.Add(numVerts);
            }

            numVerts = verts.Count;
            verts.Add(max);
            verts.Add(prevMax);
            verts.Add(prevMax + Vector3.forward * triSize);
            tris.Add(numVerts);
            tris.Add(numVerts + 2);
            tris.Add(numVerts + 1);
        }

        SightConeMesh.mesh.SetVertices(verts);
        SightConeMesh.mesh.SetTriangles(tris.ToArray(), 0);
        meshRenderer.transform.SetParent(transform, false);
        meshRenderer.transform.position += Vector3.up * .001f;
    }
}
