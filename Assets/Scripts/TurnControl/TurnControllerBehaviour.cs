﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TurnControllerBehaviour : MonoBehaviour
{
    public PlayerTurnViewModel PlayerTurn;
    public EnemyTurnViewModel EnemyTurn;

    private IEnumerator _turnCoroutine;

    public void EndTurn_Clicked()
    {
        PlayerTurn.EndTurn_Clicked();
    }

    public void PurchaseCards_Clicked()
    {
        PlayerTurn.PurchaseCards_Clicked();
    }

    public void NextUnit_Clicked()
    {
        PlayerTurn.NextUnit_Clicked();
    }

    public void ExitFloor_Clicked()
    {
        PlayerTurn.ExitFloor_Clicked();
    }

    public void StartMission(List<Archetype> archetypes)
    {
        PlayerTurn.Initialize(archetypes);
        EnemyTurn.Initialize();
        if (_turnCoroutine == null)
        {
            _turnCoroutine = TurnCoroutine();
            StartCoroutine(_turnCoroutine);
        }

        Pathfinder.Initialize(PlayerTurn, EnemyTurn);
    }

    private IEnumerator TurnCoroutine()
    {
        yield return new WaitForSeconds(.1f);
        while (true)
        {
            yield return TakeTurnCoroutine(PlayerTurn);
            yield return TakeTurnCoroutine(EnemyTurn);
        }
    }

    private IEnumerator TakeTurnCoroutine(ITurnViewModel turnViewModel)
    {
        turnViewModel.BeginTurn();

        yield return turnViewModel.TakeTurnCoroutine();

        turnViewModel.EndTurn();
    }
}
