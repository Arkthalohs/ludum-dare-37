﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class EnemyTurnViewModel : ITurnViewModel
{
    public EnemyBehaviour[] Enemies;

    public void Initialize()
    {
        Enemies = (from gameobject
               in GameObject.FindGameObjectsWithTag(Constants.EnemyLayer)
                where gameobject.GetComponent<EnemyBehaviour>()
                select gameobject.GetComponent<EnemyBehaviour>()).ToArray();
    }

    public void BeginTurn()
    {
        foreach (var enemy in Enemies)
        {
            enemy.ViewModel.BeginTurn();
        }
    }

    public IEnumerator TakeTurnCoroutine()
    {
        foreach (var enemy in Enemies)
        {
            yield return enemy.ViewModel.TakeTurnCoroutine();
        }
    }

    public void EndTurn()
    {
        foreach (var enemy in Enemies)
        {
            enemy.ViewModel.EndTurn();
        }
    }
}
