﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[Serializable]
public class PlayerTurnViewModel : ITurnViewModel
{
    public List<CharacterBehaviour> Team;
    public Transform Highlight;
    public Text MovementPointText;
    public Text EnergyPointText;
    public Button ExitFloorButton;
    public Button EndTurnButton;
    public Button PurchaseButton;
    public CharacterActionVisualizationMeshBehaviour MoveableAreaMesh;
    public LineRenderer PathLineRenderer;
    public ParticleSystem SelectedCharacterParticleSystem;
    public Text TooltipText;
    public MissionEndScreenBehaviour MissionEndScreen;
    public List<CharacterViewModel> EscapedTeamMembers = new List<CharacterViewModel>();
    public Transform SpawnPoint;
    public CharacterBehaviour CharacterPrefab;
    public Transform HandUIPanel;
    public Transform PurchasePanel;
    public Button ExitPurchaseButton;
    public Button CancelPurchaseButton;
    public Text EnergyRemainingText;
    public Text TurnCounterText;
    public int TurnCounter;

    private bool _takingTurn;
    [SerializeField]
    private CharacterBehaviour _selectedTeamMember;

    public CharacterBehaviour SelectedTeamMember
    {
        get
        {
            return _selectedTeamMember;
        }

        set
        {
            if (_selectedTeamMember != null)
            {
                _selectedTeamMember.ViewModel.SetActiveCharacter(false);
            }

            if (value != null)
            {
                value.ViewModel.SetActiveCharacter(true);
                SelectedCharacterParticleSystem.transform.SetParent(value.transform, false);
            }

            var emission = SelectedCharacterParticleSystem.emission;
            emission.enabled = value;

            MoveableAreaMesh.ActiveCharacter = value;
            _selectedTeamMember = value;
        }
    }

    public void Initialize(List<Archetype> charactersInMission)
    {
        Team = StartMission(charactersInMission);
    }

    public void BeginTurn()
    {
        if (Team.Count == 0 || Team.All(character => character.ViewModel.EntityCondition == EntityViewModel.Condition.Dead))
        {
            // Everyone's left. (Or dead) End the mission.
            MissionEndScreen.EndMission(this);
            _takingTurn = false;
            return;
        }

        foreach (var character in Team)
        {
            character.ViewModel.BeginTurn();
        }

        TurnCounter++;
        TurnCounterText.text = string.Empty + TurnCounter;
        _takingTurn = true;
    }

    public IEnumerator TakeTurnCoroutine()
    {
        while (_takingTurn)
        {
            if (SelectedTeamMember && SelectedTeamMember.ViewModel.EntityCondition != EntityViewModel.Condition.Conscious)
            {
                _selectedTeamMember = null;
                var emission = SelectedCharacterParticleSystem.emission;
                emission.enabled = false;
                MoveableAreaMesh.ActiveCharacter = null;
            }

            UpdateUI();
            if (EventSystem.current.IsPointerOverGameObject())
            {
                yield return null;
                continue;
            }

            if (Input.GetMouseButtonDown(0))
            {
                var clickRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitInfo;
                if (Physics.Raycast(clickRay, out hitInfo, 1000, LayerMask.GetMask(Constants.CharacterLayer, Constants.TreasureLayer), QueryTriggerInteraction.Ignore))
                {
                    if (hitInfo.collider.gameObject.layer == LayerMask.NameToLayer(Constants.CharacterLayer))
                    {
                        var character = hitInfo.collider.transform.parent.GetComponent<CharacterBehaviour>();
                        if (character.ViewModel.EntityCondition == EntityViewModel.Condition.Conscious)
                        {
                            SelectedTeamMember = character;
                        }
                    }
                    else if (hitInfo.collider.gameObject.layer == LayerMask.NameToLayer(Constants.TreasureLayer))
                    {
                        var treasure = hitInfo.collider.GetComponent<TreasureBehaviour>();
                        if (!treasure)
                        {
                            treasure = hitInfo.transform.parent.GetComponent<TreasureBehaviour>();
                        }

                        treasure.PickUp(SelectedTeamMember);
                    }
                }
                else
                {
                    SelectedTeamMember = null;
                }
            }

            var nullableMousePosition = SnapToGrid(MousePosition());
            Highlight.gameObject.SetActive(false);
            if (nullableMousePosition != null)
            {
                Highlight.gameObject.SetActive(true);
                var gridMousePosition = (Vector3)nullableMousePosition;
                Highlight.position = gridMousePosition;
                Highlight.position += Vector3.up * 0.01f;

                if (SelectedTeamMember && SelectedTeamMember.ViewModel.Action == CharacterViewModel.Actions.Movement)
                {
                    var path = Pathfinder.FindPath(SelectedTeamMember.transform.position, gridMousePosition, SelectedTeamMember.ViewModel.MovementPoints);
                    if (path != null)
                    {
                        var positions = path.Positions;
                        positions.Insert(0, SelectedTeamMember.transform.position);
                        PathLineRenderer.numPositions = positions.Count;
                        PathLineRenderer.SetPositions((from pos in positions select pos + Vector3.up * .05f).ToArray());
                    }
                    else
                    {
                        PathLineRenderer.numPositions = 0;
                    }

                    if (Input.GetMouseButtonDown(1))
                    {
                        SelectedTeamMember.MoveTo(gridMousePosition);
                    }
                }
                else
                {
                    PathLineRenderer.numPositions = 0;
                }
            }
            else
            {
                PathLineRenderer.numPositions = 0;
            }

            yield return null;
        }
    }

    public void EndTurn()
    {
        foreach (var character in Team)
        {
            character.ViewModel.EndTurn();
        }
    }

    public void EndTurn_Clicked()
    {
        _takingTurn = false;
    }

    public void PurchaseCards_Clicked()
    {
        if (SelectedTeamMember)
        {
            SelectedTeamMember.ViewModel.Purchase.BeginPurchase(SelectedTeamMember.ViewModel.Energy + SelectedTeamMember.ViewModel.Hand.Value);
        }
    }

    public void ExitFloor_Clicked()
    {
        // UI?
        for (var i = 0; i < Team.Count; i++)
        {
            var character = Team[i];
            if (character.ViewModel.IsInExit())
            {
                EscapedTeamMembers.Add(character.ViewModel);
                Team.Remove(character);
                SelectedCharacterParticleSystem.transform.SetParent(null, false);
                var emission = SelectedCharacterParticleSystem.emission;
                emission.enabled = false;
                SelectedCharacterParticleSystem.Clear();
                i--;
                GameObject.Destroy(character.gameObject);
            }
        }

        if (Team.Count == 0 || Team.All(character => character.ViewModel.EntityCondition == EntityViewModel.Condition.Dead))
        {
            // Everyone's left. (Or dead) End the mission.
            _takingTurn = false;
            MissionEndScreen.EndMission(this);
        }
    }

    public void NextUnit_Clicked()
    {
        for (var i = 0; i < Team.Count; i++)
        {
            if (Team[i].ViewModel.TakingTurn && Team[i].ViewModel.EntityCondition == EntityViewModel.Condition.Conscious)
            {
                SelectedTeamMember = Team[i];
                return;
            }
        }
    }

    public void UpdateUI()
    {
        if (SelectedTeamMember == null)
        {
            MovementPointText.transform.parent.gameObject.SetActive(false);
            EnergyPointText.transform.parent.gameObject.SetActive(false);
            ExitFloorButton.gameObject.SetActive(false);
            PurchaseButton.gameObject.SetActive(false);
            TooltipText.text = "Click a unit to give it orders";
        }
        else
        {
            MovementPointText.transform.parent.gameObject.SetActive(true);
            EnergyPointText.transform.parent.gameObject.SetActive(true);
            MovementPointText.text = string.Empty + SelectedTeamMember.ViewModel.MovementPoints;
            EnergyPointText.text = string.Empty + SelectedTeamMember.ViewModel.Energy;
            ExitFloorButton.gameObject.SetActive(SelectedTeamMember.ViewModel.IsInExit());
            PurchaseButton.gameObject.SetActive(SelectedTeamMember.ViewModel.TakingTurn);

            TooltipText.text = string.Empty;
            var treasures = GameObject.FindGameObjectsWithTag(Constants.TreasureLayer);
            foreach (var treasure in treasures)
            {
                if (treasure.GetComponent<TreasureBehaviour>().HasAdjacentPlayer)
                {
                    TooltipText.text = "Click the grab icon to pick the object up";
                }
            }

            if (string.IsNullOrEmpty(TooltipText.text))
            {
                switch (SelectedTeamMember.ViewModel.Action)
                {
                    case CharacterViewModel.Actions.Attack:
                        TooltipText.text = "Right click a crosshair to attack that unit";
                        break;
                    case CharacterViewModel.Actions.Movement:
                        TooltipText.text = "LMB: Play a card. RMB: Move to mouse position";
                        break;
                    case CharacterViewModel.Actions.SelectOperative:
                        TooltipText.text = "Right click an operative to select that unit";
                        break;
                    case CharacterViewModel.Actions.SelectSquare:
                        TooltipText.text = "Right click a square to select that square";
                        break;
                }
            }
        }

        var anyStillTakingTurn = false;
        foreach (var character in Team)
        {
            anyStillTakingTurn |= (character.ViewModel.TakingTurn && character.ViewModel.EntityCondition == EntityViewModel.Condition.Conscious);
        }

        EndTurnButton.gameObject.SetActive(!anyStillTakingTurn);
    }

    private List<CharacterBehaviour> StartMission(List<Archetype> archetypesToSpawn)
    {
        var characters = new List<CharacterBehaviour>();
        var positions = new List<Vector3>() {
            SpawnPoint.position - Vector3.right - Vector3.forward,
            SpawnPoint.position - Vector3.right,
            SpawnPoint.position - Vector3.right + Vector3.forward,
            SpawnPoint.position - Vector3.forward,
            SpawnPoint.position,
            SpawnPoint.position + Vector3.forward,
            SpawnPoint.position + Vector3.right - Vector3.forward,
            SpawnPoint.position + Vector3.right,
            SpawnPoint.position + Vector3.right + Vector3.forward,
        };
        foreach (var archetype in archetypesToSpawn)
        {
            var character = GameObject.Instantiate(CharacterPrefab);
            var startPosition = positions[UnityEngine.Random.Range(0, positions.Count)];
            positions.Remove(startPosition);
            character.transform.position = startPosition;
            character.Initialize(archetype, HandUIPanel, PurchasePanel, ExitPurchaseButton, CancelPurchaseButton, EnergyRemainingText);

            characters.Add(character);
        }

        return characters;
    }

    private Vector3? MousePosition()
    {
        var clickRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        if (Physics.Raycast(clickRay, out hitInfo, 1000, LayerMask.GetMask(Constants.GroundLayer), QueryTriggerInteraction.Ignore))
        {
            return hitInfo.point;
        }

        return null;
    }

    private Vector3? SnapToGrid(Vector3? nullablePosition)
    {
        if (nullablePosition == null)
        {
            return null;
        }

        var position = (Vector3)nullablePosition;
        return new Vector3(Mathf.Round(position.x), position.y, Mathf.Round(position.z));
    }
}
