﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public interface ITurnViewModel
{
    void BeginTurn();
    IEnumerator TakeTurnCoroutine();
    void EndTurn();
}
