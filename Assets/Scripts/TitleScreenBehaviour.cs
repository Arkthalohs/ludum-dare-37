﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleScreenBehaviour : MonoBehaviour
{
    public void Play_Clicked()
    {
        SceneManager.LoadScene("StartScene");
    }
}
