﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TreasureBehaviour : MonoBehaviour
{
    public SpriteRenderer GrabIcon;
    private PlayerTurnViewModel _playerTurn;

    public bool HasAdjacentPlayer
    {
        get
        {
            return GrabIcon.enabled;
        }
    }

    public void PickUp(CharacterBehaviour character)
    {
        if (character && IsAdjacent(character))
        {
            // TODO: Add UI to show card acquisation
            character.ViewModel.Hand.AddCard(CardDefinitions.Treasure);
            Destroy(gameObject);
        }
    }

    private void Awake()
    {
        _playerTurn = GameObject.Find("Game Controller").GetComponent<TurnControllerBehaviour>().PlayerTurn;
    }

    private void Update()
    {
        GrabIcon.enabled = false;
        foreach (var character in _playerTurn.Team)
        {
            if (IsAdjacent(character))
            {
                GrabIcon.enabled = true;
                GrabIcon.color = character.ViewModel.IsActiveCharacter ? Color.white : Color.gray;
            }
        }
    }

    private bool IsAdjacent(CharacterBehaviour character)
    {
        var d = (character.transform.position - transform.position);
        d.y = 0;
        return d.magnitude < 1.25f;
    }
}
