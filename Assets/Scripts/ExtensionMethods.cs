﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public enum Archetype
{
    Mastermind,
    Gadgeteer
}

public static class ExtensionMethods
{
    public static Color GetColor(this Archetype characterArchetype)
    {
        switch (characterArchetype)
        {
            case Archetype.Gadgeteer:
                return Color.green;
            case Archetype.Mastermind:
                return Color.blue;
        }

        return Color.black;
    }
}
