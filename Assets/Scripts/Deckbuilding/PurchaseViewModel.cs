﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class PurchaseViewModel
{
    public CardBehaviour CardPrefab;
    public Transform PurchasePanel;
    public List<Card> PurchasableCards;
    public Button ExitPurchaseButton;
    public Button CancelPurchaseButton;
    public Text EnergyRemainingText;

    [NonSerialized]
    private CharacterViewModel _characterViewModel;
    private int _energyToSpend;

    public void Initialize(CharacterViewModel character, List<Card> purchasableCards)
    {
        PurchasableCards = purchasableCards;
        _characterViewModel = character;
    }

    public void BuyCard(CardBehaviour card)
    {
        if (card.Card.Cost <= _energyToSpend)
        {
            CancelPurchaseButton.interactable = false;
            _energyToSpend -= card.Card.Cost;
            EnergyRemainingText.text = string.Empty + _energyToSpend;
            ExitPurchaseButton.interactable = true;
            ExitPurchaseButton.transform.GetChild(0).GetComponent<Text>().text = "Complete Purchase\nand End Character Turn";
            card.Discard();
            _characterViewModel.Hand.AddToDiscard(card.Card.Clone());

            var cardHolder = PurchasePanel.transform.GetChild(0);
            for (var i = 0; i < cardHolder.childCount; i++)
            {
                var child = cardHolder.GetChild(i).GetComponent<CardBehaviour>();
                if (child && child.Card.Cost > _energyToSpend)
                {
                    child.GetComponentInChildren<Image>().color = new Color(1, .7f, .7f);
                }
            }
        }
        else
        {
            // TODO: Show the user they have insufficient funds
        }
    }

    public void BeginPurchase(int energyToSpend)
    {
        ExitPurchaseButton.onClick.RemoveAllListeners();
        ExitPurchaseButton.onClick.AddListener(new UnityEngine.Events.UnityAction(EndPurchase_Clicked));
        ExitPurchaseButton.interactable = false;
        ExitPurchaseButton.transform.GetChild(0).GetComponent<Text>().text = "You must make at least 1 purchase";
        PurchasePanel.gameObject.SetActive(true);

        CancelPurchaseButton.interactable = true;
        CancelPurchaseButton.onClick.RemoveAllListeners();
        CancelPurchaseButton.onClick.AddListener(new UnityEngine.Events.UnityAction(CancelPurchase_Clicked));

        _energyToSpend = energyToSpend;
        EnergyRemainingText.text = string.Empty + energyToSpend;
        var cardHolder = PurchasePanel.transform.GetChild(0);
        foreach (var card in PurchasableCards)
        {
            var newCard = GameObject.Instantiate(CardPrefab, cardHolder);
            newCard.Initialize(null, card, true, BuyCard);
            if (newCard.Card.Cost > _energyToSpend)
            {
                newCard.GetComponentInChildren<Image>().color = new Color(1, .7f, .7f);
            }
        }
    }

    private void CancelPurchase_Clicked()
    {
        var cardHolder = PurchasePanel.transform.GetChild(0);
        for (var i = 0; i < cardHolder.childCount; i++)
        {
            GameObject.Destroy(cardHolder.GetChild(i).gameObject);
        }

        PurchasePanel.gameObject.SetActive(false);
    }

    private void EndPurchase_Clicked()
    {
        ExitPurchase();
    }

    private void ExitPurchase()
    {
        _characterViewModel.EndTurn();
        var cardHolder = PurchasePanel.transform.GetChild(0);
        for (var i = 0; i < cardHolder.childCount; i++)
        {
            GameObject.Destroy(cardHolder.GetChild(i).gameObject);
        }

        PurchasePanel.gameObject.SetActive(false);
    }
}
