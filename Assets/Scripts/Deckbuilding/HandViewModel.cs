﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class HandViewModel
{
    public CardBehaviour CardPrefab;
    public GameObject HandPrefab;
    public Transform UIPanel;
    public Transform HandPanel;

    [SerializeField]
    private Deck _drawPile;
    [SerializeField]
    private Deck _discardPile;
    [SerializeField]
    private List<CardBehaviour> _currentCardsInHand;
    private CharacterBehaviour _character;

    public int Value
    {
        get
        {
            var value = 0;
            foreach (var card in _currentCardsInHand)
            {
                value += card.Card.ValueIfInHand;
            }

            return value;
        }
    }

    public void Initialize(Deck startingDeck, CharacterBehaviour character)
    {
        _drawPile = startingDeck;
        _drawPile.Shuffle();
        _discardPile = new Deck();

        _currentCardsInHand = new List<CardBehaviour>();
        _character = character;
        HandPanel = GameObject.Instantiate(HandPrefab, UIPanel).transform;
        HandPanel.name = "Hand (" + character.name + ")";
        HandPanel.gameObject.SetActive(false);
        var handTransform = HandPanel.GetComponent<RectTransform>();
        handTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0, 0);
        handTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, 110, -110);
        handTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 70, -70);
        handTransform.anchorMin = Vector2.zero;
        handTransform.anchorMax = Vector2.one;
        handTransform.localScale = Vector3.one;
    }

    public void AddCard(Card pickup)
    {
        var newCard = GameObject.Instantiate(CardPrefab, HandPanel);
        newCard.Initialize(_character, pickup, false, PlayCard);
        _currentCardsInHand.Add(newCard);
    }

    public void AddToDiscard(Card discard)
    {
        _discardPile.AddCard(discard);
    }

    public bool ContainsCard(Card card)
    {
        foreach (var cardBehaviour in _currentCardsInHand)
        {
            if (cardBehaviour.Card.Equals(card))
            {
                return true;
            }
        }

        return _drawPile.ContainsCard(card) || _discardPile.ContainsCard(card);
    }

    public void DrawCards(int numToDraw = 1)
    {
        for (var i = 0; i < numToDraw; i++)
        {
            if (_drawPile.Count == 0)
            {
                // Shuffle in the discard pile
                _drawPile.ShuffleIn(_discardPile);
            }

            var card = _drawPile.DrawCard();
            AddCard(card);
        }
    }

    public void DiscardAllCards()
    {
        foreach (var card in _currentCardsInHand)
        {
            card.Card.OnDiscard(_character);
            _discardPile.AddCard(card.Card);
            card.Discard();
        }

        _currentCardsInHand.Clear();
    }

    public void PlayCard(CardBehaviour card)
    {
        if (card.Card.IsPlayable(_character))
        {
            _currentCardsInHand.Remove(card);
            _discardPile.AddCard(card.Card);
            card.Card.OnPlayed(_character);
            card.Discard();
        }
    }

    public void Destroy()
    {
        GameObject.Destroy(HandPanel.gameObject);
    }
}
