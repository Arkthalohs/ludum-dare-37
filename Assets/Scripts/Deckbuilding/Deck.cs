﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class Deck
{
    [SerializeField]
    private List<Card> Cards;

    public Deck()
    {
        Cards = new List<Card>();
    }

    public bool ContainsCard(Card card)
    {
        return Cards.Contains(card);
    }

    public static Deck StartingDeck
    {
        get
        {
            var startingDeck = new Deck();
            startingDeck.AddCard(CardDefinitions.Taser, 1);
            startingDeck.AddCard(CardDefinitions.Run, 2);
            startingDeck.AddCard(CardDefinitions.Step, 3);
            startingDeck.AddCard(CardDefinitions.Respite, 4);
            return startingDeck;
        }
    }

    public int Count
    {
        get
        {
            return Cards.Count;
        }
    }

    public Card DrawCard()
    {
        var topCard = Cards[0];
        Cards.RemoveAt(0);
        return topCard;
    }

    public void Shuffle()
    {
        var newCards = new List<Card>();
        while (Cards.Count > 0)
        {
            var i = UnityEngine.Random.Range(0, Cards.Count);
            newCards.Add(Cards[i]);
            Cards.RemoveAt(i);
        }

        Cards = newCards;
    }

    public void ShuffleIn(Deck other)
    {
        Cards.AddRange(other.Cards);
        other.Cards.Clear();
        Shuffle();
    }

    public void AddCard(Card c, int numToAdd = 1)
    {
        for (var i = 0; i < numToAdd; i++)
        {
            Cards.Add(c.Clone());
        }
        // Do we need to shuffle after every card add? Or is it fine to put every new card on the bottom?
    }
}
