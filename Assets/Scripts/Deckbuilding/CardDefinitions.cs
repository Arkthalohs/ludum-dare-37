﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class CardDefinitions
{
    public static Card Treasure
    {
        get
        {
            return new Card("The Orb", "It's shiny! And worth a lot if you get it out of here...", 0, 5)
            {
                OnPlayed = character =>
                {
                    character.ViewModel.Energy += 5;
                }
            };
        }
    }

    public static Card Step
    {
        get
        {
            return new Card("Step", "+1 Movement Point", 1, 0)
            {
                OnPlayed = character =>
                {
                    character.ViewModel.MovementPoints++;
                }
            };
        }
    }

    public static Card Respite
    {
        get
        {
            return new Card("Respite", "A little breather.", 0, 1)
            {
                OnPlayed = character =>
                {
                    character.ViewModel.Energy++;
                }
            };
        }
    }

    public static Card Run
    {
        get
        {
            return new Card("Run", "+3 Movement Points\nMakes noise as you move", 2, 0)
            {
                OnPlayed = character =>
                {
                    character.ViewModel.MovementPoints += 3;
                    character.ViewModel.MoveNoisy = true;
                },

                GetTooltip = character =>
                {
                    return "Noise will alert any guard within 3 spaces";
                }
            };
        }
    }

    public static Card Gun
    {
        get
        {
            return new Card("Gun", "Kill 1 enemy within 7 spaces", 7, 0)
            {
                OnPlayed = character =>
                {
                    character.Attack(EntityViewModel.Condition.Dead, -1, 7);
                },

                GetTooltip = character =>
                {
                    var targets = TargetCount(character, 7);
                    if (targets == 0)
                    {
                        return "There are no targets in range";
                    }
                    
                    if (targets == 1)
                    {
                        return "There is 1 target";
                    }

                    return "There are " + targets + " targets";
                },

                IsPlayable = character =>
                {
                    return TargetCount(character, 7) > 0;
                }
            };
        }
    }

    public static Card Taser
    {
        get
        {
            return new Card("Taser", "Incapacitate 1 adjacent enemy for 3 turns", 3, 0)
            {
                OnPlayed = character =>
                {
                    character.Attack(EntityViewModel.Condition.Unconscious, 3, 1);
                },

                GetTooltip = character =>
                {
                    var targets = TargetCount(character, 1);
                    if (targets == 0)
                    {
                        return "There are no targets in range";
                    }

                    if (targets == 1)
                    {
                        return "There is 1 target";
                    }

                    return "There are " + targets + " targets";
                },

                IsPlayable = character =>
                {
                    return TargetCount(character, 1) > 0;
                }
            };
        }
    }

    public static Card UtilityBelt
    {
        get
        {
            return new Card("Utility Belt", "Draw 1 Card", 1, 1)
            {
                OnPlayed = character =>
                {
                    character.ViewModel.Energy++;
                    character.ViewModel.Hand.DrawCards(1);
                }
            };
        }
    }

    public static Card Delegate
    {
        get
        {
            return new Card("Delegate", "Choose 1 operative to draw 1 card", 3, 0)
            {
                OnPlayed = character =>
                {
                    character.ChooseOperative(operative => { operative.ViewModel.Hand.DrawCards(1); });
                }
            };
        }
    }

    public static Card Command
    {
        get
        {
            return new Card("Command", "Choose 1 operative to gain 2 Movement Points", 2, 0)
            {
                OnPlayed = character =>
                {
                    character.ChooseOperative(operative => { operative.ViewModel.MovementPoints += 2; });
                }
            };
        }
    }

    public static Card Rest
    {
        get
        {
            return new Card("Rest", "Catch your breath.", 3, 2)
            {
                OnPlayed = character =>
                {
                    character.ViewModel.Energy += 2;
                }
            };
        }
    }

    public static Card Shield
    {
        get
        {
            return new Card("Shield", "If you are shot at this turn, do not take damage", 5, 1)
            {
                OnPlayed = character =>
                {
                    character.ViewModel.Energy += 1;
                    character.ViewModel.Shielded = true;
                },

                OnDiscard = character =>
                {
                    character.ViewModel.Shielded = true;
                }
            };
        }
    }

    public static Card Swapper
    {
        get
        {
            return new Card("Swapper", "Swap places with another operative or enemy", 4, 0)
            {
                OnPlayed = character =>
                {
                    character.ChooseOperative(unit =>
                    {
                        var startPos = character.transform.position;
                        character.transform.position = unit.transform.position;
                        unit.transform.position = startPos;
                    }, false);
                },

                GetTooltip = character =>
                {
                    var targets = OperativeCount(character, false, -1, true);
                    if (targets == 0)
                    {
                        return "There are no targets";
                    }

                    if (targets == 1)
                    {
                        return "There is 1 target";
                    }

                    return "There are " + targets + " targets";
                },

                IsPlayable = character =>
                {
                    return OperativeCount(character, false, -1, true) > 0;
                }
            };
        }
    }

    public static Card Shock
    {
        get
        {
            return new Card("Cattle Prod", "Incapacitate an adjacent enemy for 1 turn", 3, 0)
            {
                OnPlayed = character =>
                {
                    character.Attack(EntityViewModel.Condition.Unconscious, 1, 1);
                },

                GetTooltip = character =>
                {
                    var targets = TargetCount(character, 1);
                    if (targets == 0)
                    {
                        return "There are no targets in range";
                    }

                    if (targets == 1)
                    {
                        return "There is 1 target";
                    }

                    return "There are " + targets + " targets";
                },

                IsPlayable = character =>
                {
                    return TargetCount(character, 1) > 0;
                }
            };
        }
    }

    public static Card ChargePack
    {
        get
        {
            return new Card("Capacitor", "Energy is given to selected operative", 2, 2)
            {
                OnPlayed = character =>
                {
                    character.ChooseOperative(operative =>
                    {
                        operative.ViewModel.Energy += 2;
                    });
                }
            };
        }
    }

    public static Card Distraction
    {
        get
        {
            return new Card("Distraction", "Make a noise at a target square", 4, 0)
            {
                OnPlayed = character =>
                {
                    character.ChooseSquare(pos =>
                    {
                        character.ViewModel.MakeNoise(pos);
                    });
                }
            };
        }
    }

    public static Card Stash
    {
        get
        {
            return new Card("Supply cache", "Target operative gains 1 movement point, 1 energy, and draws 1 card", 5, 0)
            {
                OnPlayed = character =>
                {
                    character.ChooseOperative(operative =>
                    {
                        Debug.Log(operative.ViewModel.MovementPoints);
                        operative.ViewModel.MovementPoints++;
                        operative.ViewModel.Energy++;
                        operative.ViewModel.Hand.DrawCards(1);
                    });
                }
            };
        }
    }

    public static Card MasterPlan
    {
        get
        {
            return new Card("Master Plan", "All operatives draw 2 cards", 7, 0)
            {
                OnPlayed = character =>
                {
                    foreach (var operative in GameObject.FindGameObjectsWithTag(Constants.CharacterLayer))
                    {
                        var beh = operative.GetComponent<CharacterBehaviour>();
                        if (beh && beh.ViewModel.EntityCondition == EntityViewModel.Condition.Conscious)
                        {
                            beh.ViewModel.Hand.DrawCards(2);
                        }
                    }
                }
            };
        }
    }

    private static int TargetCount(CharacterBehaviour character, float range)
    {
        var targetCount = 0;
        foreach (var enemy in GameObject.FindGameObjectsWithTag(Constants.EnemyLayer))
        {
            var distance = enemy.transform.position - character.transform.position;
            if (enemy.GetComponent<EnemyBehaviour>().ViewModel.EntityCondition == EntityViewModel.Condition.Conscious &&
                Vector3.Distance(character.transform.position, enemy.transform.position) <= range + .05f &&
                !Physics.Raycast(character.transform.position, distance, distance.magnitude, LayerMask.GetMask(Constants.WallsLayer)))
            {
                targetCount++;
            }
        }

        return targetCount;
    }

    private static int OperativeCount(CharacterBehaviour character, bool allowSelf, float range, bool allowNonconcious)
    {
        var targetCount = 0;
        foreach (var operative in GameObject.FindGameObjectsWithTag(Constants.CharacterLayer))
        {
            var distance = operative.transform.position - character.transform.position;
            if (operative.GetComponent<CharacterBehaviour>() && 
                (range < 0 || Vector3.Distance(character.transform.position, operative.transform.position) <= range + .05f) &&
                (allowNonconcious || operative.GetComponent<CharacterBehaviour>().ViewModel.EntityCondition == EntityViewModel.Condition.Conscious))
            {
                if (allowSelf || operative != character.gameObject)
                {
                    targetCount++;
                }
            }
        }

        return targetCount;
    }
}