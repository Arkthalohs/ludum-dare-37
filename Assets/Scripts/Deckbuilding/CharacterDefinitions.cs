﻿using System.Collections.Generic;

public class CharacterDefinitions
{
    public static Deck MastermindStartingDeck
    {
        get
        {
            var startingDeck = new Deck();
            startingDeck.AddCard(CardDefinitions.Respite, 4);
            startingDeck.AddCard(CardDefinitions.Step, 2);
            startingDeck.AddCard(CardDefinitions.Run, 1);

            startingDeck.AddCard(CardDefinitions.Delegate, 1);
            startingDeck.AddCard(CardDefinitions.Command, 1);
            startingDeck.AddCard(CardDefinitions.ChargePack, 1);
            return startingDeck;
        }
    }

    public static List<Card> MastermindPurchases
    {
        get
        {
            var purchases = new List<Card>()
            {
                CardDefinitions.Respite,
                CardDefinitions.Run,
                CardDefinitions.Step,
                CardDefinitions.Rest,
                CardDefinitions.Delegate,
                CardDefinitions.Command,
                CardDefinitions.ChargePack,
                CardDefinitions.Distraction,
                CardDefinitions.Stash,
                CardDefinitions.MasterPlan,
            };

            purchases.Sort(new System.Comparison<Card>((c1, c2) => { return c1.Cost - c2.Cost; }));
            return purchases;
        }
    }

    public static Deck GadgeteerStartingDeck
    {
        get
        {
            var startingDeck = new Deck();
            startingDeck.AddCard(CardDefinitions.Respite, 4);
            startingDeck.AddCard(CardDefinitions.Step, 2);
            startingDeck.AddCard(CardDefinitions.Run, 1);

            startingDeck.AddCard(CardDefinitions.Taser, 1);
            startingDeck.AddCard(CardDefinitions.UtilityBelt, 1);
            startingDeck.AddCard(CardDefinitions.Shock, 1);
            return startingDeck;
        }
    }

    public static List<Card> GadgeteerPurchases
    {
        get
        {
            var purchases = new List<Card>()
            {
                CardDefinitions.Respite,
                CardDefinitions.Run,
                CardDefinitions.Step,
                CardDefinitions.Rest,
                CardDefinitions.Taser,
                CardDefinitions.UtilityBelt,
                CardDefinitions.Shield,
                CardDefinitions.Gun,
                CardDefinitions.Swapper,
                CardDefinitions.Shock
            };

            purchases.Sort(new System.Comparison<Card>((c1, c2) => { return c1.Cost - c2.Cost; }));
            return purchases;
        }
    }
}