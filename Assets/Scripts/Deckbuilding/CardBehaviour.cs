﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CardBehaviour : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public Text TitleText;
    public Text DescriptionText;
    public Card Card;
    public GameObject Cost;
    public Text CostText;
    public Text ValueText;
    public Action<CardBehaviour> OnCardClick;
    public Image TooltipRim;
    public Text TooltipText;

    private CharacterBehaviour _character;

    public void Initialize(CharacterBehaviour character, Card card, bool forPurchase, Action<CardBehaviour> onCardClick)
    {
        Card = card;
        TitleText.text = name = card.Title;
        DescriptionText.text = card.Description;
        transform.localScale = Vector3.one;
        OnCardClick = onCardClick;
        _character = character;
        TooltipText.transform.parent.gameObject.SetActive(false);
        if (card.ValueIfInHand != 0)
        {
            ValueText.text = (card.ValueIfInHand > 0 ? "+" : "-") + card.ValueIfInHand;
        }
        else
        {
            ValueText.transform.parent.gameObject.SetActive(false);
        }

        if (forPurchase)
        {
            CostText.text = string.Empty + card.Cost;
        }
        else
        {
            Cost.SetActive(false);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        transform.localScale = Vector3.one * 1.2f;

        if (_character == null)
        {
            return;
        }

        var tooltip = Card.GetTooltip(_character);
        if (!string.IsNullOrEmpty(tooltip))
        {
            TooltipText.transform.parent.gameObject.SetActive(true);
            TooltipText.text = tooltip;
            TooltipRim.color = Card.IsPlayable(_character) ? Color.green : Color.red;
        }
        else
        {
            TooltipText.transform.parent.gameObject.SetActive(false);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        transform.localScale = Vector3.one;
        TooltipText.transform.parent.gameObject.SetActive(false);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        OnCardClick(this);
    }

    public void Discard()
    {
        if (isActiveAndEnabled)
        {
            StartCoroutine(DiscardCoroutine());
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public IEnumerator DiscardCoroutine()
    {
        // Should played cards stay on screen? Porbably...
        var discardPilePos = new Vector3(Screen.width, transform.position.y);

        for (var i = 0; i < 10; i++)
        {
            transform.position = Vector3.Lerp(transform.position, discardPilePos, i / 10f);
            yield return null;
        }

        Destroy(gameObject);
    }

    private void Update()
    {
        if (TooltipText.transform.parent.gameObject.activeInHierarchy)
        {
            TooltipText.transform.parent.position = Input.mousePosition;
        }
    }
}
