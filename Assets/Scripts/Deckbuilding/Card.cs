﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class Card
{
    public string Title;
    public string Description;
    public int Cost;
    public float MovementPointsForDiscard;
    public int ValueIfInHand;

    public Action<CharacterBehaviour> OnPlayed;
    public Action<CharacterBehaviour> OnDiscard;
    public Func<CharacterBehaviour, bool> IsPlayable;
    public Func<CharacterBehaviour, string> GetTooltip;

    public Card(string title, string description, int cost, int value, float movementPointsForDiscard = 0)
    {
        Title = title;
        Description = description;
        Cost = cost;
        ValueIfInHand = value;
        IsPlayable = x => { return true; };
        OnPlayed = x => { };
        OnDiscard = x => { };
        GetTooltip = x => { return string.Empty; };
        MovementPointsForDiscard = movementPointsForDiscard;
    }

    public override bool Equals(object obj)
    {
        var card = obj as Card;
        if (card == null)
        {
            return false;
        }

        // TODO: if cards can ever be modified check for other things here?
        return card.Title == Title && card.Description == Description;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public Card Clone()
    {
        var clonedCard = new Card(Title, Description, Cost, ValueIfInHand, MovementPointsForDiscard);
        clonedCard.OnPlayed = OnPlayed;
        clonedCard.OnDiscard = OnDiscard;
        clonedCard.IsPlayable = IsPlayable;
        clonedCard.GetTooltip = GetTooltip;
        return clonedCard;
    }
}
