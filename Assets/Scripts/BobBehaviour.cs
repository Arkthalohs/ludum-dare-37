﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BobBehaviour : MonoBehaviour
{
    public int BobLength = 20;
    public float BobSpeed = .005f;

    private Vector3 _startPos;

    public void Start()
    {
        _startPos = transform.position;
        StartCoroutine(BobCoroutine());
    }

    public IEnumerator BobCoroutine()
    {
        while (true)
        {
            for (var i = 0; i < BobLength; i++)
            {
                transform.position += Vector3.up * BobSpeed;
                yield return null;
            }

            for (var i = 0; i < BobLength; i++)
            {
                transform.position += Vector3.down * BobSpeed;
                yield return null;
            }

            transform.position = _startPos;
        }
    }
}
