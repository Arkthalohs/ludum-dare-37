﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MissionEndScreenBehaviour : MonoBehaviour
{
    public Text MissionStateText;
    public Text MissionDescriptionText;
    public Text FinalScoreText;
    public Button NextButton;

    private static int _cumulativeScore;

    private MissionState _missionResolutionState;
    private PlayerTurnViewModel _playerTurn;
    private bool _missionEnded;
    private int _numOperativesLost;

    public enum MissionState
    {
        Success,
        Failure,
        Inconclusive
    }

    public void EndMission(PlayerTurnViewModel playerTurn)
    {
        if (_missionEnded)
        {
            return;
        }

        _playerTurn = playerTurn;
        _missionEnded = true;
        gameObject.SetActive(true);

        if (playerTurn.EscapedTeamMembers.Count == 0)
        {
            SetMissionState(MissionState.Failure);
        }
        else if (playerTurn.EscapedTeamMembers.Any(character => character.Hand.ContainsCard(CardDefinitions.Treasure)))
        {
            SetMissionState(MissionState.Success);
        }
        else
        {
            SetMissionState(MissionState.Inconclusive);
        }
    }

    private void SetMissionState(MissionState missionState)
    {
        _missionResolutionState = missionState;
        switch (missionState)
        {
            case MissionState.Failure:
                SetMissionFailure();
                break;
            case MissionState.Inconclusive:
                SetMissionInconclusive();
                break;
            case MissionState.Success:
                SetMissionSuccess();
                break;
            default:
                MissionStateText.text = "I̵̧̺̤͙̱̕͟N̷̡̟̜̳̖̻̯̣͘͞C͏҉͢҉̶̳͙̺̞̰̳̞̪͍̳͕̩̯Ǫ̵̭͉̺͔̲̗̩̻̹̻͔̥̯͜ͅͅͅR҉̢͏̰̲̫̠͎̕͜R̶̝͚̞̫̖̥̜̮̜̳̬͔͉͍͟͞E̸̩͎͙̞͓̺͍̹̝͉̦͇̗͙͉̬͢͝ͅͅC͘̕҉҉͏̰̥̱̼̬̝̼̰̱̖̠̥̜̮̖̫͚͙Ţ̴̢̥͖͕͈͢ ̨̡̻̣̘̫̞̯͚̭͘͞͝M̡͚̝̖͍͞ͅÍ̸̛̤̰͈͍̞̞̩̟̝͉̜̗̫̯̞̙̕ͅS̨͇͇̤̻̖̼̝̳̳̤̟̘̹͕̪̝̫̀͞͞ͅS̷̛̛̹̮̟̦̱̲̭͜I̶̷̛͔̠̪͝ͅÒ̷̦̖̺͔̝̮̩̱̯̺̺͉͚͕Ņ͏̢҉̸̩̼̖̩̗̥̮̻͈̺͚̤̜ ̸̡̖̦͖̘̙̪̦̖͇́͢S̶̞̪͖͇̫̭̱̲̙͎̬̭T̵̷̟͓̮̲͚̟̟̙̬̼͓͉͓̰̝̤͕͘Ą̸̛̺̦̩̘̗̬̻̼̘̱̜̲̬͇̫̝Ṭ͎̘͕͉̝̠͇̳͎̤͟͡E͏̨͏̭̺̗̬̥̗";
                StartCoroutine(RandomizeColorCoroutine());

                MissionDescriptionText.text = "Y̷̷̴̴͓̬͍̦͚̝̜̠̝͔̬̪O̶̶͇͎͇̫̳͖̘̕͜͡Ų̵̢͇̺̟͖̞̮̖͍̮̲͎̹͚͕͖͈̹̀͜ ̴̢̙̖̥̺̯̩͎̲ͅH͏̡̻͈͉̲̬̘̮̣͇͖̰̳͖͚̬̘̻ͅA̸̢̺̱̣̜̦̝͚̕V҉͟҉̹̲̫̠̰̲̳̮͇̲̘̼͍̺͙̣̺É̵̱̜̩̻͓̞̥͔̕͟͞ ̨̣͎̘̝͇̲̟̳͟C̶̬̙̫̭̕O̘̰̣̪͙̮͞Ṃ̶̭̩̩̣̯̬̣͎̟̀P̸҉̵̸͙̲̟͚͉̟͖̭͈̟̺͎͇̙̘̳̝͇͜ͅĻ̥͙̲̯̹̖̩͇͇͕̖̳͈̖̲͔̤̗̘͜É̢̛͕̝̼͉̼͈̟̮̼̞͢͝T̵̶̡̻̥͖͖̳̹̘̫͡Ȩ̢͈͔̼͔̲̞̤͓̀Ḑ̡̡̺̼̥͙̰̞̲͚̫͉̺̮͓̳͉̫̖̪ ̶̶͝͏̢̙͙͉̩̪͎̰T͏͖̱͙̠̩̫͎̜͢Ḩ̧̺̲͉̲̥̝͝E̵̡̧͚̮͕͚̭̱̻͎̤͎̗͍̗͈͍̪̪ ͟҉̹͙͍͚̗̳M̳̩͓̪̬̠̪͓͙͢͢ͅI͠҉̘̞͙͎̝̮̱͈͙̯͟ͅS̵̴̥͎͇̮͚̰̯̯̭͉̳̪̜̣̣̺̟ͅS̸̨̤̣͎̬̬̘̫̫̙̬ͅI̧̡͓̲̬̪̹̮̳̰̺͕̳̥̠͈̼̲͎O͜҉̺̳̮̯͈̝͚̤͈̘̞̮̹̤̻̭̯̀͟͟Ǹ̴̮͓̞̞͈̜̯̤̦͟͠ ̨̨̺̳̘̖̭͙̪͎̩̲͎̹̩͖̻W̧͏͉̠͍̘̺̗̘͓Í̷̵̸̻̙͖͇͔̻͉͓̘͉́T̩̼̱͖̰͚̞̹̙̣̖̟͙̙͙̠̜̠̼́͟͡H͏̵̞̜͇̮̭ͅ ̵̷̡͈̤̻͖͈̘̰̮̭̰͎̩͘͞À̗͍͍̦͔͕̦̜̮̥̲̞̠̭̱̺͎̣͘̕͟N̡͇̫͓̩̟̭̗͟ͅ ̵̨̡̜̝͓͓̥͚͚̪͖̠̬̥̝͔͢ͅͅU̧̯̖̥̘̬̦̫͝͠N̶͓͓͖̱̣͔̝͔͓͓̻͟͡ͅR͢͏̨̻͚͇̼͍̩̳̩̦̠̜̠͖̖͇̣͙̀͞Ḙ̢̛̹̪̲̪̟̪̩̻̩̠͚̠̕ͅC̴͏̶̴̙͕̼͈̱͍̰̳̪͔O̴̸̸̧̖̺͉̪͔̙̞͡G̛̜̟̳̣̖̥͞Ǹ̵̛͎̲̼̦̥͕̰͍͔̞̭̪̮̬̻͕͠͠I͏̵̺̹̺͕̘̺̮͍̜͚̟͍̲̥̬͟ͅZ̢͎͕̺͎̜͚̟̝͓͉̬̣͈̥̠̝ͅĘ̡̩̩̳̯͍͍͙̱̳̜̯͙̬̩͍̀͜D͓̥̯̯͕͎̱̟̙̩͘͠͞ ̠̰̦͉̦̜͖̻̕̕ͅS͏҉̼͇͖͚͚̜̬̠̠̠̻͈̯ͅͅT̷̴̨̝̮̻͓̗̫͎̭̱̩͔͇̫̱͍͉͇͢ͅÀ̶̱̣͎̩̤̹̠͖̹̣͍̲̯̕͜T̸͖͓͍̹̝̠̙̯͉̤̀͝E̴̷͎͖̣̝̼̬̱̞̺̲̞̳̙̟̯͟.̨͎̯̹̦͕͙͍̼̲̟͓̱͓̻́͟ͅ ̸͕͚̩̰̀T͏̶̧̲̳̝̺̫̖̠͘ͅU̵̢͉̬͇̜͉̗͖̜̙̦̦͖͈̳̖̕ͅͅR̨̹͕̦̹̹̲̀̕͝͞ͅN̴̡̮͎̹͎̲̯̦̦̟̦͔͇̝̣̦̻̖͡ͅ ̴̶͏̛͔͍̹̙͕̹̺̰̘̬̦͡B͚̮̩͉̭̺̠̣̗̘̪̙̠́̀͜͡A̴̡̘̩͓͍͎͓̞͈͖̙͢͢͠Ç̸̷̱̹̰͖̣K̸̘͕̝̠̫̳̺̰̱̯͕̤̖̰̳̲̭͜ ͚̩͇̭̮́N̶̘̦̦̙̥̫̗̟̦̪͉͇͘̕͝͡O͏҉̸͓̖̘͓͓̠̤̼͍̲͓͈͔͍̘̜̟͢͟W҉̴͓̩̲͖̯̳̤̯̜͎͙̗͓ ̨̛͘҉͏͈̹̗̣̗̰̘͇͖̟̦͙̘̫̖̠̠̘̠B͏҉̜̜̫̣̘̫͔̤̫̺̀͟͠ͅĘ͘͏̠͕͓̣̲̖̫͍̻͕͕̯̺̱ͅͅF̨̦̟̩̝̱̯̯O̶҉̶̷̯̙͈͉̘̩̜̤͉̰̞͔̗͖̣̙̺͕̻̀R͏̗̥̘͉̠͇͇͚̪̙̖͇̮͕̱̀̀È̸͠҉̩͙̦̪̖̱̣̕ ̴̱̫̳͚̳͍̫͖̻̟̮̗̘͍̖̯̠̀̕͟͝ͅͅÌ̸̱̩̩̪͔͢͞T͞҉̦͓̮͚̣͚̙̹̼̲̖̦̹̺ ̸̢̫͈͕̪̣̤͇͙̱̲͖͜͡Ḭ̴̜̥̱̗̤̲͚͍̜̳̞̫͜ͅŞ̛͇͉̟̠͢͞͝ ̴̨̯̗̮̼͎͢͡͝T҉̴͉̝̜͙̙̤͉̰̕͞O̵̰͚̫̭̞͎̼̤̳̲͎͍͇̪͢͝O̢̢̢̱͍̰̝̹͖̳̖̭̼̞̮̼̺̪͔̤̤͍͟͞ ̥̳͚͓̼͉̯̞̺̲͕̲̲̣̀́͘̕ͅL͏͎͎̝̙͚͡A̡͚̰̺̠̦̬T̸̴͏̼̜̫͖̖͚̺̺͘E͜҉̴̛͉͇͚͇͈̰̖̗͘ͅ.̧̛͏҉̛̝̪̫ͅ ̨͠͏̣͇̬̠̞̮͈͓̩̩̫̩̯̫̯͍̳́ͅͅY̵̙̻͙̘͉̮̭̺͓̮̜͍̦̞̭͚̬͘͘O̢͇̝̳̰̯̭̻̭͈͍͘Ư̵̷̮̹̝͈͔̪͎̫̱̺͙͇̘̫̞ͅ ̵̩͚̺̘̻̀͡H̕͏̞̭̗̠͈͔͍̟̫̀A̵͟҉̷͕̬̱̙̳̮̭͔̘̘̘͜ͅͅͅV̴̢̧̧̭̖̺͎̖̳̜E̵͍͈͕͇̳̖̥͍̯̗̟͉̜͎̣͘͝͡ ҉̧͉̠͎̼̝̬̯̟̪͈̩͖̝M̡̢̟͖̹̖͓̤̫͕͇̪̖̟̰͈̻̜̭͔̕ͅA̡͏̵͕̝̮̫͚̩͈͓͎̬̺D҉̧͚̬͔̱E͏̷̻̲͓̯̰̩̩̺̺̬̱̙̕͠ͅ ̧̤̞̣͙̭̺͍̺̩A̵̛̰͎͕͎͚̬̣̻͘͝ ̵̝̪̩̝̜̲̭̩͜͡Ǵ̻̱̫̯̹͚̲̼̱̬͉̙͉̗̖͝ͅR̛͓͖̪͕͙͖͕͚̬͕̦͚̩̱͢A҉̗̗͓̮̳̰͇̤̬͠V͏̸͟҉̳̗̙̭̞̪̙͈̥̳̻̤̰͉Ę̶̝͇̩̖͙͎͠͝͞ͅ ̛͠͏̰̪̱̺̦̜̬͈͕̮͘ͅM҉̢͇̬̖̪̠͇̫͟͟͡I̴̫͙̞̯̝̥̼̱̘̣͖̕͜͟ͅS̷̫͖̲̱̩̪͚͍͙̩͈͔͍̫̝̗͇͓͜͞T̴̢̢͇͈̭͈̩̳̕͡Á̝̺̜̲̠͍͡Ḳ̛͕̰̜̳̮̀͘͜͡E̝̥̫̺̱̲͖̙̝̮͖̙̲̗͙̪̘͢͝͞.̸̖̲͔͖͖́͘͘ ̴̳͔͇͖̠̗͙̠̭͓͍̬̟̯͈̻̲̰͓͞Y̵̷̸̡͈̟͎͔̮͖͓̠̙͠Ó̦͇̳̦͖̯͙̳̬̣͢͟͜ͅU̵̸͉̪̣̯͈͙͖̗̺̕ ̛̻̹̥̟̯̳̬̳̺̳̗̼͈̜͈̗͜͞ͅH͔͉̱͎͔͈̫͙̼̖̜̮́̕͡͞À̛̙͙̖̫̥̪͕̤̜̱͓͈̖̺͍̪̦́͟V̡̙̜̖̼̦̬̟͎̮̞̥̗͇̰̫͉͉̀́E͏̸̡҉̢̥̦̗͍͓̘̙̦̬͉̼̳̗͙̪ ̝͔̱̤̳͕̮̕͜͠D̨̲̖̭̙̠̝̭̭̖̮̬͙͉̕͘͟͢O̴̷̤̭̫̰͈͈͔͍̫̹̙̝͝͡͞O̢̧̰̜͉͔͙̣̠͎̥̦̠̱͉͍͕̮͟͝ͅM̴̧̪͚͉̪͍͓̭͘͟E̢̧̛̦̟̗̹͇̖̞͎̗̟͡͡D͏̧̛͓̦̯̭̦͈̭ ̶̢͕͔̯͔̜̞̱͔̭̯͟U̷̷̹͙̮̩̞͔̫̼͙̱̖̕͝S̢̫͉̩̻̙͈͘͘͞͞ ̢͖̣͖̣̩̞͕̹̣͜Ạ̷̴͕̬̥̤͕̭̣̰̳̘̭̕͞L̴̨̩̻̜̳̹̩͕̪͉͝ͅL̟̜͖̰̟͇̠͉̬̼̳͈͚̞̩͔̣͝.̸̯̗͔̙̠͎͉̱̺̞̫̥̹̜̼̞͙͕̮͡";
                break;
        }

        var finalScore = -10 * _playerTurn.TurnCounter;
        finalScore -= 50 * _numOperativesLost;
        if (missionState == MissionState.Success)
        {
            finalScore += 100;
        }

        _cumulativeScore += finalScore;
        FinalScoreText.text += _cumulativeScore;
    }

    private void SetMissionFailure()
    {
        MissionStateText.text = "Mission Failure";
        MissionStateText.color = Color.red;

        MissionDescriptionText.text = "All operatives in the field were lost. This is unacceptable.";

        AddLostOperativesToDescription();
        NextButton.onClick.RemoveAllListeners();
        NextButton.onClick.AddListener(new UnityEngine.Events.UnityAction(() => { Application.Quit(); }));
        NextButton.GetComponentInChildren<Text>().text = "End Game";
    }

    private void SetMissionSuccess()
    {
        MissionStateText.text = "Mission Success";
        MissionStateText.color = Color.green;

        MissionDescriptionText.text = "You fulfilled your objective and escaped with the valuables in hand. Nicely done!";

        AddLostOperativesToDescription();
        NextButton.onClick.RemoveAllListeners();

        AddNextMissionButton();
    }

    private void SetMissionInconclusive()
    {
        MissionStateText.text = "Mission Results Inconclusive";
        MissionStateText.color = Color.yellow;

        MissionDescriptionText.text = "You made it out of that hellhole, but don't have the goods in hand. While not an all-out failure, this sort of result is not acceptable if repeated.";

        AddLostOperativesToDescription();

        AddNextMissionButton();
    }

    private void NextLevel(MissionControlBehaviour missionController)
    {
        missionController.DisplayNextMission((from op in _playerTurn.EscapedTeamMembers select op.Archetype).ToList());
    }

    private void AddNextMissionButton()
    {
        NextButton.onClick.RemoveAllListeners();

        var missionController = GameObject.Find("Mission Controller").GetComponent<MissionControlBehaviour>();
        if (!missionController.HasNextLevel)
        {
            NextButton.onClick.AddListener(new UnityEngine.Events.UnityAction(() => Application.Quit()));
            NextButton.GetComponentInChildren<Text>().text = "End Game";
        }
        else
        {
            NextButton.onClick.AddListener(new UnityEngine.Events.UnityAction(() => { NextLevel(missionController); }));
            NextButton.GetComponentInChildren<Text>().text = "Next Mission";
        }
    }

    private void AddLostOperativesToDescription()
    {
        var lostOperatives = string.Empty;
        _numOperativesLost = 0;
        foreach (var operative in _playerTurn.Team)
        {
            if (operative.ViewModel.EntityCondition == EntityViewModel.Condition.Dead)
            {
                _numOperativesLost++;
                lostOperatives += operative.name + ", ";
            }
        }

        if (!string.IsNullOrEmpty(lostOperatives))
        {
            MissionDescriptionText.text += "\n\nOperatives Lost: " + lostOperatives.Substring(0, lostOperatives.Length - 2);
        }
    }

    private void Start()
    {
        gameObject.SetActive(false);
    }

    private IEnumerator RandomizeColorCoroutine()
    {
        while (true)
        {
            var target = Random.ColorHSV();
            for (var i = 0; i < 20f; i++)
            {
                MissionStateText.color = Color.Lerp(MissionStateText.color, target, i / 20f);
                yield return null;
            }
        }
    }
}
