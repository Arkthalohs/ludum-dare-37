﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MissionControlBehaviour : MonoBehaviour
{
    public int MissionIndex;
    public GameObject MissionDisplayCanvas;
    public Text MissionTitle;
    public Text MissionDescription;
    public Text MissionObjective;
    public GameObject MissionOperatives;
    public Text OperativePrefab;
    public Image MissionImage;
    private List<Archetype> _archetypes;
    private List<string[]> Missions = new List<string[]>
    {
        new string[] {
            "Test",
            "The Orb of Balthazar",
            "There is one room in the Museum of Modern Valuables that is of particular interest. In it rests the Orb of Balthazar, a gigantic floating orb made of solid gold. It is on display with surprisingly little security in a small museum in the middle of the city. If we can get ahold of it, it could serve as discretionary spending for a great number of future capers.",
            "Objective: Retrieve the Orb from the vault room inside the building. With the treasure in hand, get to the exit and escape with it."
        },

        new string[] {
            "Level2",
            "The Cube of Thaddeus",
            "The next score isn't much different. The same rules apply--get in, get the goods, get out, but this time it's not an orb that we're after. We've heard rumors that there's a solid gold Cube floating about in one of these businesses. But after the antics we pulled last time, it's sure to be better guarded.",
            "Objective: Retrieve the Cube from the vault room inside the building. With the treasure in hand, get to the exit and escape with it."
        }
    };

    public bool HasNextLevel
    {
        get
        {
            return MissionIndex < Missions.Count;
        }
    }

    public void DisplayNextMission(List<Archetype> archetypes)
    {
        if (MissionIndex > 0)
        {
            SceneManager.LoadScene("Empty");
        }

        _archetypes = archetypes;
        MissionDisplayCanvas.SetActive(true);
        MissionTitle.text = Missions[MissionIndex][1];
        MissionDescription.text = Missions[MissionIndex][2];
        MissionObjective.text = Missions[MissionIndex][3];
        MissionImage.sprite = Resources.Load<Sprite>(Missions[MissionIndex][0]);

        for (var i = 0; i < MissionOperatives.transform.childCount; i++)
        {
            Destroy(MissionOperatives.transform.GetChild(i).gameObject);
        }

        foreach (var archetype in _archetypes)
        {
            var operative = Instantiate(OperativePrefab, MissionOperatives.transform);
            operative.text = archetype.ToString();
            operative.color = archetype.GetColor();
        }
    }

    public void NextMission_Clicked()
    {
        SceneManager.sceneLoaded += FinishLoad;

        MissionDisplayCanvas.SetActive(false);
        SceneManager.LoadScene(Missions[MissionIndex][0]);
        MissionIndex++;
    }

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
        MissionIndex = 0;
        DisplayNextMission(new List<Archetype>() { Archetype.Gadgeteer, Archetype.Mastermind });
    }

    private void FinishLoad(Scene scene, LoadSceneMode mode)
    {
        SceneManager.sceneLoaded -= FinishLoad;
        GameObject.Find("Game Controller").GetComponent<TurnControllerBehaviour>().StartMission(_archetypes);
    }
}
