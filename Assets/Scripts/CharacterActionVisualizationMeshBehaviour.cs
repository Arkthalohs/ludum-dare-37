﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CharacterActionVisualizationMeshBehaviour : MonoBehaviour
{
    public CharacterBehaviour ActiveCharacter;

    private MeshFilter _reachableSquaresMesh;
    private MeshRenderer _meshRenderer;
    private Dictionary<GameObject, GameObject> _crosshairs;

    private void Start()
    {
        _reachableSquaresMesh = GetComponent<MeshFilter>();
        _meshRenderer = GetComponent<MeshRenderer>();
    }

    private void Update()
    {
        _meshRenderer.enabled = ActiveCharacter;
        if (ActiveCharacter)
        {
            switch (ActiveCharacter.ViewModel.Action)
            {
                case CharacterViewModel.Actions.Attack:
                    if (_crosshairs == null)
                    {
                        _crosshairs = new Dictionary<GameObject, GameObject>();
                    }

                    UpdateMeshForViableAttacks();
                    break;
                case CharacterViewModel.Actions.Movement:
                    if (_crosshairs != null)
                    {
                        foreach (var crosshair in _crosshairs.Values)
                        {
                            GameObject.Destroy(crosshair);
                        }
                    }

                    _crosshairs = null;
                    UpdateMeshForViableMovement();
                    break;
                case CharacterViewModel.Actions.SelectSquare:
                case CharacterViewModel.Actions.SelectOperative:
                    _reachableSquaresMesh.mesh = new Mesh();
                    break;
            }
        }
        else if (_crosshairs != null)
        {
            foreach (var crosshair in _crosshairs.Values)
            {
                GameObject.Destroy(crosshair);
            }

            _crosshairs = null;
        }
    }

    private void UpdateMeshForViableAttacks()
    {
        var verts = new List<Vector3>();
        var tris = new List<int>();

        _reachableSquaresMesh.mesh = new Mesh();
        _reachableSquaresMesh.mesh.SetVertices(verts);
        _reachableSquaresMesh.mesh.SetTriangles(tris, 0);

        foreach (var enemy in GameObject.FindGameObjectsWithTag(Constants.EnemyLayer))
        {
            var distance = enemy.transform.position - ActiveCharacter.transform.position;
            if (enemy.GetComponent<EnemyBehaviour>().ViewModel.EntityCondition == EntityViewModel.Condition.Conscious &&
                Vector3.Distance(ActiveCharacter.transform.position, enemy.transform.position) <= ActiveCharacter.ViewModel.AttackRange + .05f &&
                !Physics.Raycast(ActiveCharacter.transform.position, distance, distance.magnitude, LayerMask.GetMask(Constants.WallsLayer)))
            {
                if (!_crosshairs.ContainsKey(enemy))
                {
                    var crosshair = Instantiate(Resources.Load<GameObject>("Crosshair"), enemy.transform);
                    crosshair.GetComponentInChildren<SpriteRenderer>().color = Color.grey;
                    _crosshairs[enemy] = crosshair;
                    _crosshairs[enemy].transform.SetParent(enemy.transform, false);
                    _crosshairs[enemy].transform.localPosition = Vector3.zero;
                }

                AddSquareToMesh(enemy.transform.position, tris, verts);
            }
        }

        _reachableSquaresMesh.mesh = new Mesh();
        _reachableSquaresMesh.mesh.SetVertices(verts);
        _reachableSquaresMesh.mesh.SetTriangles(tris, 0);

        var colors = new List<Color>(verts.Count);
        for (var i = 0; i < verts.Count; i++)
        {
            colors.Add(new Color(.7f, 0, .05f, .6f));
        }

        _reachableSquaresMesh.mesh.SetColors(colors);
    }

    private void UpdateMeshForViableMovement()
    {
        var verts = new List<Vector3>();
        var tris = new List<int>();

        var reachable = Pathfinder.ReachableSquares(ActiveCharacter.transform.position, ActiveCharacter.ViewModel.MovementPoints, false);

        foreach (var node in reachable)
        {
            AddSquareToMesh(node, tris, verts);
        }

        _reachableSquaresMesh.mesh = new Mesh();
        _reachableSquaresMesh.mesh.SetVertices(verts);
        _reachableSquaresMesh.mesh.SetTriangles(tris, 0);

        var colors = new List<Color>(verts.Count);
        for (var i = 0; i < verts.Count; i++)
        {
            colors.Add(new Color(.05f, 0, .7f, .6f));
        }

        _reachableSquaresMesh.mesh.SetColors(colors);
    }

    private void AddSquareToMesh(Pathfinder.Node node, List<int> tris, List<Vector3> verts)
    {
        AddSquareToMesh(new Vector3(node.X, 0, node.Z), tris, verts);
    }

    private void AddSquareToMesh(Vector3 squareCenter, List<int> tris, List<Vector3> verts)
    {
        tris.Add(AddPosition(new Vector3(squareCenter.x - .5f, 0, squareCenter.z - .5f), verts));
        tris.Add(AddPosition(new Vector3(squareCenter.x + .5f, 0, squareCenter.z - .5f), verts));
        tris.Add(AddPosition(new Vector3(squareCenter.x - .5f, 0, squareCenter.z + .5f), verts));

        tris.Add(AddPosition(new Vector3(squareCenter.x + .5f, 0, squareCenter.z + .5f), verts));
        tris.Add(AddPosition(new Vector3(squareCenter.x + .5f, 0, squareCenter.z - .5f), verts));
        tris.Add(AddPosition(new Vector3(squareCenter.x - .5f, 0, squareCenter.z + .5f), verts));
    }

    private int AddPosition(Vector3 position, List<Vector3> vertices)
    {
        if (vertices.Contains(position))
        {
            var index = vertices.IndexOf(position);
            return index;
        }

        vertices.Add(position);
        return vertices.Count - 1;
    }
}
