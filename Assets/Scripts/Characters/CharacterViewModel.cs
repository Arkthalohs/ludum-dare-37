﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class CharacterViewModel : EntityViewModel
{
    public HandViewModel Hand;
    public PurchaseViewModel Purchase;
    public bool TakingTurn;
    public bool MoveNoisy;
    public float AttackRange;
    public Actions Action;
    public int Energy;
    public Archetype Archetype;
    public bool Shielded;

    private bool _isActiveCharacter;
    private Color _color;

    public enum Actions
    {
        Movement,
        Attack,
        SelectOperative,
        SelectSquare
    }

    public bool IsActiveCharacter
    {
        get
        {
            return _isActiveCharacter;
        }
    }

    public void Initialize(Archetype archetype, CharacterBehaviour character, Deck startingDeck, List<Card> purchasableCards)
    {
        // I don't like passing the character down like this because it's very much not a VM/V thing to do...
        Hand.Initialize(startingDeck, character);
        Purchase.Initialize(this, purchasableCards);
        base.Initialize(character.transform);
        _color = _transform.GetComponentInChildren<Renderer>().material.color;
        Archetype = archetype;
    }

    internal IEnumerator ChooseSquareCoroutine(Action<Vector3> chosen)
    {
        Action = Actions.SelectSquare;

        // This is awful.
        AttackRange = 10000;
        yield return null;
        while (true)
        {
            var nullableMousePosition = SnapToGrid(MousePosition());
            if (nullableMousePosition != null)
            {
                var gridMousePosition = (Vector3)nullableMousePosition;

                if (Input.GetMouseButton(1))
                {
                    chosen(gridMousePosition);
                    yield return null;
                    Action = Actions.Movement;
                    yield break;
                }
            }

                yield return null;
        }

        Action = Actions.Movement;
    }

    internal IEnumerator ChooseOperativeCoroutine(Action<CharacterBehaviour> actionToTake, bool allowSelf)
    {
        Action = Actions.SelectOperative;

        // This is awful.
        AttackRange = 10000;
        yield return null;
        while (true)
        {
            var targets = GameObject.FindGameObjectsWithTag(Constants.CharacterLayer);
            if (IsActiveCharacter && targets.Length == 0)
            {
                // No valid targets
                // TODO: Potentially surface this to the player?
                break;
            }

            foreach (var target in targets)
            {
                var character = target.GetComponent<CharacterBehaviour>();
                if (character)
                {
                    character.GetComponentInChildren<Renderer>().material.color = character.ViewModel.Archetype.GetColor();
                    RaycastHit hitInfo;
                    if ((allowSelf || target != _transform.gameObject)
                        &&Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo, 1000, LayerMask.GetMask(Constants.CharacterLayer))
                        && hitInfo.collider.transform.parent.gameObject.Equals(character.gameObject))
                    {
                        target.GetComponentInChildren<Renderer>().material.color = character.ViewModel.Archetype.GetColor() * 2f;
                        if (Input.GetMouseButton(1))
                        {
                            target.GetComponentInChildren<Renderer>().material.color = character.ViewModel.Archetype.GetColor();
                            actionToTake(character);
                            yield return null;
                            Action = Actions.Movement;
                            yield break;
                        }
                    }
                }
            }

            yield return null;
        }

        Action = Actions.Movement;
    }

    // THis isn't working. Not sure why. I'll just limit to operatives or enemies, not both.
    ////internal IEnumerator ChooseUnitCoroutine(Action<GameObject> actionToTake)
    ////{
    ////    Action = Actions.SelectOperative;

    ////    // This is awful.
    ////    AttackRange = 10000;
    ////    yield return null;
    ////    while (true)
    ////    {
    ////        var targetObjs = GameObject.FindGameObjectsWithTag(Constants.CharacterLayer);
    ////        targetObjs = targetObjs.Union(GameObject.FindGameObjectsWithTag(Constants.EnemyLayer)).ToArray();
    ////        var targets = targetObjs.ToDictionary(a => a, a => a.GetComponentInChildren<Renderer>().material.color);
    ////        Debug.Log(targets.Count);
    ////        if (IsActiveCharacter && targets.Count == 0)
    ////        {
    ////            // No valid targets
    ////            // TODO: Potentially surface this to the player?
    ////            break;
    ////        }

    ////        foreach (var target in targets.Keys)
    ////        {
    ////            target.GetComponentInChildren<Renderer>().material.color = targets[target];
    ////            RaycastHit hitInfo;
    ////            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo, 1000, LayerMask.GetMask(Constants.CharacterLayer))
    ////                && hitInfo.collider.transform.parent.gameObject.Equals(target))
    ////            {
    ////                target.GetComponentInChildren<Renderer>().material.color = targets[target] * 2f;
    ////                if (Input.GetMouseButton(1))
    ////                {
    ////                    actionToTake(target);
    ////                    yield return null;
    ////                    Action = Actions.Movement;
    ////                    yield break;
    ////                }
    ////            }
    ////        }

    ////        yield return null;
    ////    }

    ////    Action = Actions.Movement;
    ////}

    public IEnumerator AttackCoroutine(Condition conditionDealt, int turnsIncapacitated, float range)
    {
        Action = Actions.Attack;

        // This is awful.
        AttackRange = range;
        yield return null;
        while (true)
        {
            var targets = GameObject.FindGameObjectsWithTag(Constants.TargetLayer);
            if (IsActiveCharacter && targets.Length == 0)
            {
                // No valid targets
                // TODO: Potentially surface this to the player?
                break;
            }

            foreach (var target in targets)
            {
                target.GetComponent<SpriteRenderer>().color = Color.grey;
                RaycastHit hitInfo;
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo, 1000, LayerMask.GetMask(Constants.TargetLayer))
                    && hitInfo.collider.transform.gameObject.Equals(target.gameObject))
                {
                    target.GetComponent<SpriteRenderer>().color = Color.red;
                    if (Input.GetMouseButton(1))
                    {
                        target.GetComponentInParent<EnemyBehaviour>().ViewModel.Attacked(conditionDealt, turnsIncapacitated);
                        yield return null;
                        Action = Actions.Movement;
                        yield break;
                    }
                }
            }

            yield return null;
        }

        Action = Actions.Movement;
    }

    public void BeginTurn()
    {
        // Set to 1 to allow for shifting.
        MovementPoints = 1;
        TakingTurn = true;
        _transform.GetComponentInChildren<Renderer>().material.color = _color;
        _actionCoroutine = null;
        MoveNoisy = false;
        Energy = 0;
        Shielded = false;

        Hand.DrawCards(5);
    }

    public void EndTurn()
    {
        TakingTurn = false;
        _transform.GetComponentInChildren<Renderer>().material.color = _color / 4;
        MovementPoints = 0;
        Energy = 0;
        Hand.DiscardAllCards();
        if (_actionCoroutine != null)
        {
            _transform.GetComponent<MonoBehaviour>().StopCoroutine(_actionCoroutine);
        }

        Action = Actions.Movement;
        _actionCoroutine = null;
    }

    public override void Attacked(Condition conditionDealt, int turnsIncapacitated)
    {
        if (EntityCondition == Condition.Conscious && !Shielded)
        {
            base.Attacked(conditionDealt, turnsIncapacitated);
            SetActiveCharacter(false);
        }
    }

    public void SetActiveCharacter(bool isActive)
    {
        _isActiveCharacter = isActive;
        Hand.HandPanel.gameObject.SetActive(isActive);
    }

    public bool IsInExit()
    {
        RaycastHit hitInfo;
        var hit = Physics.Raycast(_transform.position, Vector3.down, out hitInfo, 2, LayerMask.GetMask(Constants.GroundLayer), QueryTriggerInteraction.Ignore);
        return hit && hitInfo.collider.gameObject.tag == Constants.ExitTag;
    }

    public void Destroy()
    {
        Hand.Destroy();
    }

    private Vector3? MousePosition()
    {
        var clickRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        if (Physics.Raycast(clickRay, out hitInfo, 1000, LayerMask.GetMask(Constants.GroundLayer), QueryTriggerInteraction.Ignore))
        {
            return hitInfo.point;
        }

        return null;
    }

    private Vector3? SnapToGrid(Vector3? nullablePosition)
    {
        if (nullablePosition == null)
        {
            return null;
        }

        var position = (Vector3)nullablePosition;
        return new Vector3(Mathf.Round(position.x), position.y, Mathf.Round(position.z));
    }
}
