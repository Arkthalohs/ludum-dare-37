﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Pathfinder
{
    private static PlayerTurnViewModel Player;
    private static EnemyTurnViewModel Enemy;

    public static void Initialize(PlayerTurnViewModel player, EnemyTurnViewModel enemy)
    {
        Player = player;
        Enemy = enemy;
    }

    public static List<Node> ReachableSquares(Vector3 startPos, float maxDistance, bool allowDiagonals)
    {
        var squares = new List<Node>();
        var openSet = new List<Node>() { new Node(startPos) };
        var distance = new Dictionary<Node, float>();
        distance[openSet[0]] = 0;
        while (openSet.Count > 0)
        {
            var cur = openSet[0];
            openSet.RemoveAt(0);

            if (distance[cur] <= maxDistance && !squares.Contains(cur))
            {
                squares.Add(cur);
                foreach (var neighbor in GetNeighbors(cur, allowDiagonals))
                {
                    var d = distance[cur] + cur.DistanceTo(neighbor);
                    if (d <= maxDistance && !squares.Contains(neighbor) && !neighbor.IsInvalid())
                    {
                        distance[neighbor] = d;
                        openSet.Add(neighbor);
                    }
                }
            }
        }

        squares.RemoveAll(node => node.ContainsPlayer());
        return squares;
    }

    public static Path FindPath(Vector3 startPos, Vector3 goalPos, float maxDistance = -1, bool allowDiagonals = false)
    {
        if (maxDistance == 0)
        {
            return null;
        }

        var start = new Node(startPos);
        var goal = new Node(goalPos);
        var closedSet = new List<Node>();
        var openSet = new List<Node>() { start };

        var cameFrom = new Dictionary<Node, Node>();
        var gScore = new Dictionary<Node, float>();
        gScore[start] = 0;
        var fScore = new Dictionary<Node, float>();
        fScore[start] = HeuristicEstimate(start, goal);
        while (openSet.Count > 0)
        {
            openSet = openSet.OrderBy(node => fScore[node]).ToList();
            var current = openSet[0];
            if (current.Equals(goal))
            {
                var path = new Path(ReconstructPath(cameFrom, current), fScore[current]);
                if (new Node(goalPos).ContainsPlayer())
                {
                    path.RemoveLast();
                }

                return path;
            }

            openSet.Remove(current);
            closedSet.Add(current);

            foreach (var neighbor in GetNeighbors(current, allowDiagonals))
            {
                if (closedSet.Contains(neighbor) || neighbor.IsInvalid())
                {
                    continue;
                }

                var tentativeGScore = gScore[current] + 1;
                if (neighbor.IsDiagonal(current))
                {
                    tentativeGScore += .5f;
                }

                if (neighbor.IsWatched(Enemy.Enemies))
                {
                    // TODO: Have player avoid the enemy vision cone if there's a valid path that avoids it.
                }

                if (maxDistance > 0 && tentativeGScore > maxDistance)
                {
                    continue; // Too far. Don't track this.
                }
                if (!openSet.Contains(neighbor))
                {
                    openSet.Add(neighbor);
                }
                else if (gScore.ContainsKey(neighbor) && tentativeGScore >= gScore[neighbor])
                {
                    continue; // Not a better path
                }

                // Best path!
                cameFrom[neighbor] = current;
                gScore[neighbor] = tentativeGScore;
                fScore[neighbor] = gScore[neighbor] + HeuristicEstimate(neighbor, goal);
            }
        }

        return null;
    }

    private static float HeuristicEstimate(Node start, Node goal)
    {
        return Mathf.Abs(start.X - goal.X) + Mathf.Abs(start.Z - goal.Z);
    }

    private static List<Node> ReconstructPath(Dictionary<Node, Node> cameFrom, Node current)
    {
        var totalPath = new List<Node> { current };
        while (cameFrom.ContainsKey(current))
        {
            current = cameFrom[current];
            totalPath.Add(current);
        }

        totalPath.Reverse();
        return totalPath;
    }

    private static List<Node> GetNeighbors(Node target, bool allowDiagonals)
    {
        var neighbors = new List<Node>();
        neighbors.Add(new Node(target.X - 1, target.Z));
        neighbors.Add(new Node(target.X + 1, target.Z));
        neighbors.Add(new Node(target.X, target.Z - 1));
        neighbors.Add(new Node(target.X, target.Z + 1));

        // Diagonals
        if (allowDiagonals)
        {
            neighbors.Add(new Node(target.X - 1, target.Z - 1));
            neighbors.Add(new Node(target.X - 1, target.Z + 1));
            neighbors.Add(new Node(target.X + 1, target.Z - 1));
            neighbors.Add(new Node(target.X + 1, target.Z + 1));
        }

        return neighbors;
    }

    public class Path
    {
        public float Length;

        private List<Node> _nodePath;

        public Path(List<Node> positions, float length)
        {
            _nodePath = positions;
            Length = length;
        }

        public List<Vector3> Positions
        {
            get
            {
                var positions = (from node in _nodePath select new Vector3(node.X, 0, node.Z)).ToList();
                if (positions.Count > 0)
                {
                    positions.RemoveAt(0);
                }

                return positions;
            }
        }

        public float DistanceToSecond()
        {
            return _nodePath[0].DistanceTo(_nodePath[1]);
        }

        public void TrimToLength(float maxLength)
        {
            if (Length <= maxLength)
            {
                return;
            }

            var newLength = 0f;
            var newPath = new List<Node>();
            var current = _nodePath[0];
            newPath.Add(current);
            var i = 1;
            while (newLength + current.DistanceTo(_nodePath[i]) <= maxLength)
            {
                newPath.Add(_nodePath[i]);
                newLength += current.DistanceTo(_nodePath[i]);
                i++;
                current = _nodePath[i];
            }

            Length = newLength;
            _nodePath = newPath;
        }

        public void RemoveLast()
        {
            if (_nodePath.Count > 1)
            {
                Length -= _nodePath[_nodePath.Count - 2].DistanceTo(_nodePath[_nodePath.Count - 1]);
                _nodePath.RemoveAt(_nodePath.Count - 1);
            }
        }
    }

    public class Node
    {
        public int X;
        public int Z;

        public Node(int x, int z)
        {
            X = x;
            Z = z;
        }

        public Node(Vector3 position)
        {
            X = Mathf.RoundToInt(position.x);
            Z = Mathf.RoundToInt(position.z);
        }

        public float DistanceTo(Node other)
        {
            // Assuming adjacent for now...
            return 1 + (IsDiagonal(other) ? .5f : 0);
        }

        public bool IsDiagonal(Node other)
        {
            return other.X != X && other.Z != Z;
        }

        public bool IsWatched(IEnumerable<EnemyBehaviour> enemies)
        {
            foreach (var enemy in enemies)
            {
                if (enemy.ViewModel.CanSee(new Vector3(X, 0, Z)))
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsInvalid()
        {
            // TODO: This feels like it will be slow with lots of walls. Consider passing in a map.
            return !Physics.Raycast(new Vector3(X, 1.5f, Z), Vector3.down, 2, LayerMask.GetMask(Constants.GroundLayer)) ||
                Physics.Raycast(new Vector3(X, 1.5f, Z), Vector3.down, 2, LayerMask.GetMask(Constants.WallsLayer));
        }

        public bool ContainsPlayer()
        {
            return Physics.Raycast(new Vector3(X, 3f, Z), Vector3.down, 4, LayerMask.GetMask(Constants.CharacterLayer));
        }

        public override bool Equals(object obj)
        {
            return obj is Node && (((Node)obj).X == X) && (((Node)obj).Z == Z);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return "{" + X + "," + Z + "}";
        }
    }
}
