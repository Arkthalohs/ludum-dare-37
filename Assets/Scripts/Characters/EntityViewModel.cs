﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class EntityViewModel
{
    public float MovementPoints;
    public Condition EntityCondition;
    public Text IncapacitatedCountText;

    protected Transform _transform;
    protected IEnumerator _actionCoroutine;
    protected ParticleSystem _noisePrefab;
    protected int _incapacitatedTurns;

    private float _noiseRange = 3;

    public enum Condition
    {
        Conscious,
        Unconscious,
        Dead
    }

    public virtual void Initialize(Transform character)
    {
        _noisePrefab = Resources.Load<ParticleSystem>("NoiseParticleSystem");
        _transform = character;
    }

    public IEnumerator TakeAction(IEnumerator action)
    {
        if (_actionCoroutine == null)
        {
            _actionCoroutine = action;
            yield return action;
            _actionCoroutine = null;
        }
        else
        {
            Debug.LogWarning("Trying to do two things at once!");
        }
    }

    public IEnumerator MoveToCoroutine(Vector3 targetPosition, float speed, bool moveNoise)
    {
        yield return MoveToCoroutine(Pathfinder.FindPath(_transform.position, targetPosition, MovementPoints), speed, moveNoise);
    }

    public IEnumerator MoveToCoroutine(Pathfinder.Path path, float speed, bool moveNoise)
    {
        if (path == null || path.Length > MovementPoints)
        {
            // No valid path found. Should we have some kind of indicator that the move failed?
            yield break;
        }

        var positions = path.Positions;
        while (positions.Count > 0)
        {
            var frames = 10f / speed;
            MovementPoints -= path.DistanceToSecond();
            _transform.rotation = Quaternion.LookRotation(positions[0] - _transform.position, _transform.up);
            for (var i = 0; i < frames; i++)
            {
                _transform.position = Vector3.Lerp(_transform.position, positions[0], i / frames);
                yield return null;
            }

            if (moveNoise)
            {
                MakeNoise(_transform.position);
            }

            if (ShouldCancelMove())
            {
                yield break;
            }

            yield return new WaitForSeconds(.02f);
            positions.RemoveAt(0);
        }
    }

    public virtual void Attacked(Condition conditionDealt, int turnsIncapacitated)
    {
        if (EntityCondition == Condition.Conscious)
        {
            EntityCondition = conditionDealt;
            if (conditionDealt == Condition.Unconscious)
            {
                _incapacitatedTurns = turnsIncapacitated;
            }

            _transform.position -= Vector3.up;
        }
    }

    public void MakeNoise(Vector3 position)
    {
        SpawnParticle(_noisePrefab, .5f, position);
        foreach (var enemy in GameObject.FindGameObjectsWithTag(Constants.EnemyLayer))
        {
            if (Vector3.Distance(enemy.transform.position, position) <= _noiseRange + Mathf.Epsilon)
            {
                enemy.GetComponent<EnemyBehaviour>().ViewModel.NotifyOfSound(position);
            }
        }
    }

    protected void SpawnParticle(ParticleSystem particle, float lifetime, Vector3 source)
    {
        _transform.GetComponent<MonoBehaviour>().StartCoroutine(SpawnAndKillParticle(particle, lifetime, source));
    }

    protected IEnumerator SpawnAndKillParticle(ParticleSystem particle, float lifetime, Vector3 source)
    {
        var noise = GameObject.Instantiate(particle);
        noise.transform.position = source;

        yield return new WaitForSeconds(lifetime);
        GameObject.Destroy(noise.gameObject);
    }

    protected virtual bool ShouldCancelMove()
    {
        return false;
    }
}
