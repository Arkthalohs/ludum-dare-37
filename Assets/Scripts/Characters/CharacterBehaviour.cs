﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterBehaviour : MonoBehaviour
{
    public CharacterViewModel ViewModel = new CharacterViewModel();
    private ParticleSystem _shieldEffect;

    public void MoveTo(Vector3 targetPosition)
    {
        StartCoroutine(ViewModel.TakeAction(ViewModel.MoveToCoroutine(targetPosition, 2, ViewModel.MoveNoisy)));
    }

    public void Attack(EntityViewModel.Condition conditionDealt, int turnsIncapacitated, float range)
    {
        StartCoroutine(ViewModel.TakeAction(ViewModel.AttackCoroutine(conditionDealt, turnsIncapacitated, range)));
    }

    public void ChooseOperative(Action<CharacterBehaviour> actionToTake, bool allowSelf = true)
    {
        StartCoroutine(ViewModel.TakeAction(ViewModel.ChooseOperativeCoroutine(actionToTake, allowSelf)));
    }

    public void ChooseSquare(Action<Vector3> actionToTake)
    {
        StartCoroutine(ViewModel.TakeAction(ViewModel.ChooseSquareCoroutine(actionToTake)));
    }

    ////public void ChooseUnit(Action<GameObject> action)
    ////{
    ////    StartCoroutine(ViewModel.TakeAction(ViewModel.ChooseUnitCoroutine(action)));
    ////}

    public void Initialize(Archetype characterArchetype, 
        Transform handUIPanel, Transform purchasePanel, Button exitPurchaseButton,
        Button cancelPurchaseButton, Text energyRemainingText)
    {
        GetComponentInChildren<Renderer>().material.color = characterArchetype.GetColor();
        name = characterArchetype.ToString();
        ViewModel.Hand.UIPanel = handUIPanel;
        ViewModel.Purchase.PurchasePanel = purchasePanel;
        ViewModel.Purchase.ExitPurchaseButton = exitPurchaseButton;
        ViewModel.Purchase.CancelPurchaseButton = cancelPurchaseButton;
        ViewModel.Purchase.EnergyRemainingText = energyRemainingText;

        // TODO: Do the purchasable card stuff better.
        // Probably not going to happen.
        List<Card> purchasableCards = null;
        Deck startingDeck = null;
        switch (characterArchetype)
        {
            case Archetype.Gadgeteer:
                purchasableCards = CharacterDefinitions.GadgeteerPurchases;
                startingDeck = CharacterDefinitions.GadgeteerStartingDeck;
                break;
            case Archetype.Mastermind:
                purchasableCards = CharacterDefinitions.MastermindPurchases;
                startingDeck = CharacterDefinitions.MastermindStartingDeck;
                break;
        }

        ViewModel.Initialize(characterArchetype, this, startingDeck, purchasableCards);
    }

    private void Update()
    {
        if (!_shieldEffect)
        {
            _shieldEffect = GetComponentInChildren<ParticleSystem>();
        }

        var emission = _shieldEffect.emission;
        emission.enabled = ViewModel.Shielded;
    }

    private void OnDestroy()
    {
        ViewModel.Destroy();
    }
}
